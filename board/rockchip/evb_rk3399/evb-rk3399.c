/*
 * (C) Copyright 2016 Rockchip Electronics Co., Ltd
 *
 * SPDX-License-Identifier:     GPL-2.0+
 */

#include <common.h>
#include <dm.h>
#include <misc.h>
#include <ram.h>
#include <dm/pinctrl.h>
#include <dm/uclass-internal.h>
#include <asm/setup.h>
#include <asm/arch/periph.h>
#include <power/regulator.h>
#include <u-boot/sha256.h>
#include <usb.h>
#include <dwc3-uboot.h>
#include <spl.h>
#include <bootretry.h>
#include <cli.h>
#include <command.h>
#include <console.h>
#include <environment.h>
#include <errno.h>
#include <i2c.h>
#include <malloc.h>
#include <asm/byteorder.h>
#include <linux/compiler.h>

DECLARE_GLOBAL_DATA_PTR;

#define RK3399_CPUID_OFF  0x7
#define RK3399_CPUID_LEN  0x10

#define EEPROM_BUFFER_SIZE 1024
#define SN_LEN  64
#define HEADER_SIZE 12
#define EEPROM_ADDRESS "0x50"
#define EEPROM_DATA_START_ADDRESS "0"

#ifdef CONFIG_DM_I2C
#define DEFAULT_ADDR_LEN	(-1)
#else
#define DEFAULT_ADDR_LEN	1
#endif

enum i2c_err_op {
	I2C_ERR_READ,
	I2C_ERR_WRITE,
};

static uint8_t eeprom_buffer[EEPROM_BUFFER_SIZE];

extern uint get_alen(char *arg, int default_len);
extern int i2c_get_cur_bus_chip(uint chip_addr, struct udevice **devp);
extern int i2c_report_err(int ret, enum i2c_err_op op);
extern int cmd_i2c_set_bus_num(unsigned int busnum);

int rk_board_init(void)
{
	struct udevice *pinctrl, *regulator;
	int ret;

	/*
	 * The PWM does not have decicated interrupt number in dts and can
	 * not get periph_id by pinctrl framework, so let's init them here.
	 * The PWM2 and PWM3 are for pwm regulators.
	 */
	ret = uclass_get_device(UCLASS_PINCTRL, 0, &pinctrl);
	if (ret) {
		debug("%s: Cannot find pinctrl device\n", __func__);
		goto out;
	}

	/* Enable pwm0 for panel backlight */
	ret = pinctrl_request_noflags(pinctrl, PERIPH_ID_PWM0);
	if (ret) {
		debug("%s PWM0 pinctrl init fail! (ret=%d)\n", __func__, ret);
		goto out;
	}

	ret = pinctrl_request_noflags(pinctrl, PERIPH_ID_PWM2);
	if (ret) {
		debug("%s PWM2 pinctrl init fail!\n", __func__);
		goto out;
	}

	ret = pinctrl_request_noflags(pinctrl, PERIPH_ID_PWM3);
	if (ret) {
		debug("%s PWM3 pinctrl init fail!\n", __func__);
		goto out;
	}

	ret = regulator_get_by_platname("vcc5v0_host", &regulator);
	if (ret) {
		debug("%s vcc5v0_host init fail! ret %d\n", __func__, ret);
		goto out;
	}

	ret = regulator_set_enable(regulator, true);
	if (ret) {
		debug("%s vcc5v0-host-en set fail!\n", __func__);
		goto out;
	}

out:
	return 0;
}

static void setup_macaddr(void)
{
#if CONFIG_IS_ENABLED(CMD_NET)
	int ret;
	const char *cpuid = env_get("cpuid#");
	u8 hash[SHA256_SUM_LEN];
	int size = sizeof(hash);
	u8 mac_addr[6];

	/* Only generate a MAC address, if none is set in the environment */
	if (env_get("ethaddr"))
		return;

	if (!cpuid) {
		debug("%s: could not retrieve 'cpuid#'\n", __func__);
		return;
	}

	ret = hash_block("sha256", (void *)cpuid, strlen(cpuid), hash, &size);
	if (ret) {
		debug("%s: failed to calculate SHA256\n", __func__);
		return;
	}

	/* Copy 6 bytes of the hash to base the MAC address on */
	memcpy(mac_addr, hash, 6);

	/* Make this a valid MAC address and set it */
	mac_addr[0] &= 0xfe;  /* clear multicast bit */
	mac_addr[0] |= 0x02;  /* set local assignment bit (IEEE802) */
	eth_env_set_enetaddr("ethaddr", mac_addr);
#endif

	return;
}

static void setup_serial(void)
{
#if CONFIG_IS_ENABLED(ROCKCHIP_EFUSE)
	struct udevice *dev;
	int ret, i;
	u8 cpuid[RK3399_CPUID_LEN];
	u8 low[RK3399_CPUID_LEN/2], high[RK3399_CPUID_LEN/2];
	char cpuid_str[RK3399_CPUID_LEN * 2 + 1];
	u64 serialno;
	char serialno_str[16];

	/* retrieve the device */
	ret = uclass_get_device_by_driver(UCLASS_MISC,
					  DM_GET_DRIVER(rockchip_efuse), &dev);
	if (ret) {
		debug("%s: could not find efuse device\n", __func__);
		return;
	}

	/* read the cpu_id range from the efuses */
	ret = misc_read(dev, RK3399_CPUID_OFF, &cpuid, sizeof(cpuid));
	if (ret) {
		debug("%s: reading cpuid from the efuses failed\n",
		      __func__);
		return;
	}

	memset(cpuid_str, 0, sizeof(cpuid_str));
	for (i = 0; i < 16; i++)
		sprintf(&cpuid_str[i * 2], "%02x", cpuid[i]);

	debug("cpuid: %s\n", cpuid_str);

	/*
	 * Mix the cpuid bytes using the same rules as in
	 *   ${linux}/drivers/soc/rockchip/rockchip-cpuinfo.c
	 */
	for (i = 0; i < 8; i++) {
		low[i] = cpuid[1 + (i << 1)];
		high[i] = cpuid[i << 1];
	}

	serialno = crc32_no_comp(0, low, 8);
	serialno |= (u64)crc32_no_comp(serialno, high, 8) << 32;
	snprintf(serialno_str, sizeof(serialno_str), "%llx", serialno);

	env_set("cpuid#", cpuid_str);
	env_set("serial#", serialno_str);
#endif

	return;
}

int misc_init_r(void)
{
	setup_serial();
	setup_macaddr();

	return 0;
}

int custom_macaddr_serial(void)
{
	uint	chip;
	uint	addr;
	int alen, ret, j;

	struct udevice *dev;

	uint32_t position = 0;
	uint32_t atom_data_crc_length = 0;

	uint8_t mac_addr[6];
	uint8_t tmp[20];
	char serialno_str[SN_LEN];

	// eeprom is on I2C-0
	cmd_i2c_set_bus_num(0);

	/*
	 * I2C chip address
	 */
	chip = simple_strtoul(EEPROM_ADDRESS, NULL, 16);

	/*
	 * I2C data address within the chip.  This can be 1 or
	 * 2 bytes long.  Some day it might be 3 bytes long :-).
	 */
	addr = simple_strtoul("0", NULL, 16);
	alen = get_alen("0", DEFAULT_ADDR_LEN);
	if (alen > 3)
		return CMD_RET_USAGE;

	ret = i2c_get_cur_bus_chip(chip, &dev);
	if (!ret && alen != -1)
		ret = i2c_set_chip_offset_len(dev, alen);
	if (ret)
		return i2c_report_err(ret, I2C_ERR_READ);

	ret = dm_i2c_read(dev, addr, eeprom_buffer, EEPROM_BUFFER_SIZE);

	position = HEADER_SIZE;

	for (j=1; j<5; j++) {
		atom_data_crc_length = 0;
		position = position + 2 + 2;
		atom_data_crc_length = (((eeprom_buffer[position+3])<<24)
			+ ((eeprom_buffer[position+2])<<16) + ((eeprom_buffer[position+1])<<8)
			+ ((eeprom_buffer[position])));
		position = position + 4 + atom_data_crc_length;
	}

	position = position - atom_data_crc_length;

	memset(serialno_str, 0, SN_LEN);
	serialno_str[0] = eeprom_buffer[position];
	serialno_str[1] = eeprom_buffer[position+1];
	serialno_str[2] = eeprom_buffer[position+2];
	serialno_str[3] = eeprom_buffer[position+3];
	serialno_str[4] = eeprom_buffer[position+4];
	serialno_str[5] = eeprom_buffer[position+5];
	serialno_str[6] = eeprom_buffer[position+6];
	serialno_str[7] = eeprom_buffer[position+7];
	serialno_str[8] = eeprom_buffer[position+8];
	serialno_str[9] = eeprom_buffer[position+9];
	serialno_str[10] = eeprom_buffer[position+10];
	serialno_str[11] = eeprom_buffer[position+11];
	serialno_str[12] = eeprom_buffer[position+12];
	serialno_str[13] = eeprom_buffer[position+13];
	serialno_str[14] = eeprom_buffer[position+14];
	serialno_str[15] = eeprom_buffer[position+15];
	serialno_str[16] = eeprom_buffer[position+16];
	serialno_str[17] = eeprom_buffer[position+17];
	serialno_str[18] = eeprom_buffer[position+18];
	serialno_str[19] = eeprom_buffer[position+19];
	serialno_str[20] = eeprom_buffer[position+20];
	serialno_str[21] = eeprom_buffer[position+21];
	serialno_str[22] = eeprom_buffer[position+22];
	serialno_str[23] = eeprom_buffer[position+23];

	env_set("serial#", serialno_str);
	printf("serial#=%s\n", env_get("serial#"));

	position = position + SN_LEN;
	mac_addr[0] = eeprom_buffer[position];
	mac_addr[1] = eeprom_buffer[position+1];
	mac_addr[2] = eeprom_buffer[position+2];
	mac_addr[3] = eeprom_buffer[position+3];
	mac_addr[4] = eeprom_buffer[position+4];
	mac_addr[5] = eeprom_buffer[position+5];

	sprintf((char *)tmp, "%02x:%02x:%02x:%02x:%02x:%02x",
		mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4],
		mac_addr[5]);

	env_set("ethaddr", (char *)tmp);
	printf("ethaddr=%s\n", env_get("ethaddr"));

	return 0;
}

int rk_board_late_init(void)
{
	custom_macaddr_serial();

	return 0;
}

#ifdef CONFIG_SERIAL_TAG
void get_board_serial(struct tag_serialnr *serialnr)
{
	char *serial_string;
	u64 serial = 0;

	serial_string = env_get("serial#");

	if (serial_string)
		serial = simple_strtoull(serial_string, NULL, 16);

	serialnr->high = (u32)(serial >> 32);
	serialnr->low = (u32)(serial & 0xffffffff);
}
#endif

#ifdef CONFIG_USB_DWC3
static struct dwc3_device dwc3_device_data = {
	.maximum_speed = USB_SPEED_HIGH,
	.base = 0xfe800000,
	.dr_mode = USB_DR_MODE_PERIPHERAL,
	.index = 0,
	.dis_u2_susphy_quirk = 1,
	.usb2_phyif_utmi_width = 16,
};

int usb_gadget_handle_interrupts(void)
{
	dwc3_uboot_handle_interrupt(0);
	return 0;
}

int board_usb_init(int index, enum usb_init_type init)
{
	return dwc3_uboot_init(&dwc3_device_data);
}
#endif
